#!/bin/bash

bumpType=""

shopt -s nocasematch

if [[ $CI_COMMIT_MESSAGE =~ ^patch.+$ ]]; then
    bumpType="patch"
elif [[ $CI_COMMIT_MESSAGE =~ ^minor.+$ ]]; then
    bumpType="minor"
elif [[ $CI_COMMIT_MESSAGE =~ ^major.+$ ]]; then
    bumpType="major"
fi

shopt -u nocasematch

bumpUp() {
    RE='[^0-9]*\([0-9]*\)[.]\([0-9]*\)[.]\([0-9]*\)\([0-9A-Za-z-]*\)'

    step="$1"
    if [ -z "$1" ]
    then
    step=patch
    fi

    base="$2"

    MAJOR=`echo $base | sed -e "s#$RE#\1#"`
    MINOR=`echo $base | sed -e "s#$RE#\2#"`
    PATCH=`echo $base | sed -e "s#$RE#\3#"`

    case "$step" in
    major)
            MAJOR=$(($MAJOR+1))
        ;;
    minor)
            MINOR=$(($MINOR+1))
        ;;
    patch)
            PATCH=$(($PATCH+1))
        ;;
    esac

    echo "v$MAJOR.$MINOR.$PATCH"
}

PREV=$(cat README.md | grep go\ install\ .*@v[0-9]\.[0-9]\.[0-9] | cut -d '@' -f2)
NEW=$(bumpUp $bumpType $PREV)
sed -i "s/@$PREV/@$NEW/g" README.md
echo $NEW
