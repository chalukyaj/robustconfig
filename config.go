/*
Copyright © 2023 Chalukya J chalukyaj@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package robustconfig

import (
	"fmt"
	"os"

	"github.com/pelletier/go-toml/v2"
)

type RobustConfig struct {
	Config       map[string]interface{}
	ConfigFiles  []string
	WorkingDir   string
	EnvVarPrefix string
}

func (rc *RobustConfig) AddConfigFile(ConfigFile string) {
	rc.ConfigFiles = append(rc.ConfigFiles, ConfigFile)
}

func (rc *RobustConfig) AddCurrentDirectory(WorkingDir string) {
	rc.WorkingDir = WorkingDir
}

func (rc *RobustConfig) UpdateEnvVarPrefix(Prefix string) {
	if Prefix == "" {
		panic("Invalid value for `Prefix` {empty}")
	}
	rc.EnvVarPrefix = Prefix
}

func (rc *RobustConfig) Init() {
	fileToml := make(map[string]map[string]interface{})

	profile := os.Getenv(rc.EnvVarPrefix + "_PROFILE")
	if profile == "" {
		rc.fetchTomlProfileData(fileToml, []string{"default"}, rc.ConfigFiles[0])
		rc.updateConfig(nil, "", true)
		return
	}

	for ind, ConfigFile := range rc.ConfigFiles {
		if ind == 0 {
			rc.fetchTomlProfileData(fileToml, []string{"default", profile}, ConfigFile)
			continue
		}
		rc.fetchTomlProfileData(fileToml, []string{profile}, ConfigFile)
	}
	rc.updateConfig(nil, "", true)
}

func (rc *RobustConfig) fetchTomlProfileData(fileToml map[string]map[string]interface{}, Profiles []string, ConfigFile string) {
	file, err := os.ReadFile(rc.WorkingDir + ConfigFile)
	if err != nil {
		panic(err)
	}

	err = toml.Unmarshal([]byte(file), &fileToml)
	if err != nil {
		panic(err)
	}

	for _, Profile := range Profiles {
		if Profile == "default" {
			rc.Config = fileToml[Profile]
			continue
		}
		rc.updateConfig(fileToml, Profile, false)
	}
}

func (rc *RobustConfig) updateConfig(fileToml map[string]map[string]interface{}, profile string, updateWithEnv bool) {
	var profileData map[string]interface{}
	var ok bool

	if fileToml != nil {
		profileData, ok = fileToml[profile]
		if !ok {
			return
		}
	} else {
		profileData = rc.Config
	}

	for k, v := range profileData {
		if updateWithEnv {
			osVal := os.Getenv(rc.EnvVarPrefix + "_" + k)
			if osVal != "" {
				rc.Config[k] = osVal
			}
			continue
		}

		if _, ok := rc.Config[k]; !ok {
			continue
		}
		rc.Config[k] = v
	}
}

func (rc *RobustConfig) _get(key string, shouldPanic bool) (interface{}, error) {
	val, ok := rc.Config[key]
	if !ok {
		err := fmt.Errorf("Cannot find key : `%s`", key)
		if shouldPanic {
			panic(err)
		}
		return nil, err
	}
	return val, nil
}

func (rc *RobustConfig) Get(key string) (interface{}, error) {
	return rc._get(key, false)
}

func (rc *RobustConfig) GetP(key string) interface{} {
	val, _ := rc._get(key, true)
	return val
}

func (rc *RobustConfig) GetString(key string) string {
	val, _ := rc._get(key, true)
	return val.(string)
}

func (rc *RobustConfig) GetInt(key string) int {
	val, _ := rc._get(key, true)
	return int(val.(int64))
}

func (rc *RobustConfig) GetMapStrInterface(key string) map[string]interface{} {
	val, _ := rc._get(key, true)
	return val.(map[string]interface{})
}

func (rc *RobustConfig) GetArrInterface(key string) []interface{} {
	val, _ := rc._get(key, true)
	return val.([]interface{})
}

func (rc *RobustConfig) Set(key string, value interface{}) {
	rc.Config[key] = value
}

func (rc *RobustConfig) Del(key string) error {
	_, ok := rc.Config[key]
	if !ok {
		return fmt.Errorf("Cannot find key")
	}

	delete(rc.Config, key)

	return nil
}
