# RobustConfig
## _The only config manager your golang application needs._


RobustConfig is a config manager which supports **multiple profiles** which can be switched between via a single environment variable.

##### Features
- Multiple Profiles each with it's own set of config values
- Prevent accidentally leaving a config unitialized for any environment by making sure all config values have to be in `[default]` section of the config toml
- Add multiple config files to support environment specific config files (in case your config file is really long and you'd like to split it to multiple files).
- Ability to change the environment variable prefix that will be used by the environment overrides to avoid conflict with any existing env vars
- Cli tool to generate initial boilerplate and placeholder config file for faster setup

##### Installation
```bash
go install gitlab.com/chalukyaj/robustconfig/robustconfig-cli@v1.2.2
go get gitlab.com/chalukyaj/robustconfig@v1.2.2
```

##### Usage
```bash
#This will create `settings.toml` in the current directory and `config/config.go`. The default env prefix will be `ENV`.
robustconfig-cli init

#This will create `config.toml` in the current directory and `config/config.go`. The default env prefix will be `RC_VAR`.
robustconfig-cli init -f config -p RC_VAR
```

##### Config File Structure and commands to get config values
Your config file will be a `toml` file with the following structure
```text
[default]
TEST="default"
SECRET=""

[dev]
TEST="devvalue"
SECRET="secretval"

[prod]
...
```
##### _Notes_
- In this example toml `Config.Get("TEST")` will give `default` when `{ENV_PREFIX}_PROFILE` is not set
- If for example when `{ENV_PREFIX}_PROFILE` is set the same code snippet will output `devvalue`
- This can be further overriden via envvars, for eg, `export {ENV_PREFIX}_TEST="ENVVALUE"` will override the value for `Config.Get("TEST")` to be `ENVVALUE`
- The other sections such as `[dev]` and `[prod]` can be part of seperate files which can be added via `Config.AddConfigFile(<dev|prod.toml>)` in the `config/config.go` file
- You can even add something like `.secrets.toml` file as an additional config which won't be committed to source control


##### Changelogs
- v1.2.2/v1.2.1
    - Started returning dummy value from config Init method so that config initialization can be done at a global level if needed
- v1.2.0
    - Added new methods `GetP`, `GetString`, `GetInt`, `GetMapStrInterface`, `GetArrInterface`
    - These methods allow users to get config value directly without having to typecast them (except for `GetP`) and don't return an error and instead `panic`
    - This is done with the assumption that as long as there is a default value for KEY in config toml, these methods won't run into a key missing error
- v1.1.0
    - Added CI to auto release tags
    - Improvements in README
- v1.0.0
    - Released for PUBLIC
    - Added README.md for documenting the library
    - Improvements to the readability of the CLI output
- v0.1.3
    - Renamed cli tool to `robustconfig-cli` to avoid confusing it with the library itself
- v0.1.2
    - Renamed library and cli tool to `robustconfig`
- v0.1.1
    - Added proper support to handle the relative path for `settings.toml`
- v0.1.0
    - Initial Library

## License

This code is distributed under the GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 license, see the [LICENSE](https://gitlab.com/chalukyaj/robustconfig/-/blob/main/LICENSE) file.
