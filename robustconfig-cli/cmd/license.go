/*
Copyright © 2023 Chalukya J chalukyaj@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"
	"io"
	"os"

	"github.com/spf13/cobra"
)

func showWarranty(warranty bool, copying bool, full bool) {
	if warranty {
		fmt.Println("\t" + `THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY
APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT
HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY
OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM
IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF
ALL NECESSARY SERVICING, REPAIR OR CORRECTION.`)
	} else if copying {
		fmt.Println("\t" + `To protect your rights, we need to prevent others from denying you
these rights or asking you to surrender the rights.  Therefore, you have
certain responsibilities if you distribute copies of the software, or if
you modify it: responsibilities to respect the freedom of others.
	
	For example, if you distribute copies of such a program, whether
gratis or for a fee, you must pass on to the recipients the same
freedoms that you received.  You must make sure that they, too, receive
or can get the source code.  And you must show them these terms so they
know their rights.`)
	} else if full {
		file, err := os.Open("../LICENSE")
		if err != nil {
			panic(err)
		}
		defer func() {
			if err = file.Close(); err != nil {
				panic(err)
			}
		}()

		b, err := io.ReadAll(file)
		fmt.Print("\n" + string(b))
	}
}

// licenseCmd represents the license command
var licenseCmd = &cobra.Command{
	Use:   "license",
	Short: "Show License related information for the `robustconfig` cli tool",
	Run: func(cmd *cobra.Command, args []string) {
		warranty, _ := cmd.Flags().GetBool("warranty")
		copying, _ := cmd.Flags().GetBool("copying")
		full, _ := cmd.Flags().GetBool("full")
		if warranty || copying || full {
			showWarranty(warranty, copying, full)
			return
		}
		cmd.Help()
	},
}

func init() {
	rootCmd.AddCommand(licenseCmd)

	licenseCmd.Flags().BoolP("warranty", "w", false, "show source code copying related clause of the license")
	licenseCmd.Flags().BoolP("copying", "c", false, "show warranty disclaimer")
	licenseCmd.Flags().BoolP("full", "f", false, "show full license")
}
