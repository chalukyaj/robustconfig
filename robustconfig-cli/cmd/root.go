/*
Copyright © 2023 Chalukya J chalukyaj@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "robustconfig",
	Short: "RobustConfig Cli tool for initializing/managing config for your GoLang app",
	Long: "\t" + `RobustConfig Copyright © 2023 Chalukya J
This program comes with ABSOLUTELY NO WARRANTY; for details type 'robustconfig license -w'.
This is free software, and you are welcome to redistribute it
under certain conditions; type 'robustconfig license -c' for details.`,
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
}
