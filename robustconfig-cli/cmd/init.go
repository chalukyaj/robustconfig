/*
Copyright © 2023 Chalukya J chalukyaj@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

// initCmd represents the init command
var initCmd = &cobra.Command{
	Use:   "init",
	Short: "Intialize config for your GoLang App",
	Run: func(cmd *cobra.Command, args []string) {
		file, _ := cmd.Flags().GetString("file")
		prefix, _ := cmd.Flags().GetString("envprefix")
		if file == "" || prefix == "" {
			panic("file/envprefix cannot be empty")
		}
		var err error
		if err = os.MkdirAll("./config", 0766); err != nil {
			panic(err)
		}

		var settingsFile *os.File
		if settingsFile, err = os.Create("./" + file + ".toml"); err != nil {
			panic(err)
		}
		settingsFile.WriteString(`[default]

[dev]

[prod]
`)
		settingsFile.Close()

		var configFile *os.File
		if configFile, err = os.Create("./config/config.go"); err != nil {
			panic(err)
		}
		configFile.WriteString(
			fmt.Sprintf(`package config

import (
	"os"
	"gitlab.com/chalukyaj/robustconfig"
)

var Config = robustconfig.RobustConfig{}

func Init() bool {
	if len(Config.ConfigFiles) > 0 && Config.ConfigFiles[0] == "%s.toml" {
		return false
	}

	cwd, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	Config.AddCurrentDirectory(cwd + "/")
	Config.AddConfigFile("%s.toml")
	Config.UpdateEnvVarPrefix("%s")

	/*
	Uncomment the line below to add an additional file '.secrets.toml' for config
	which should be typically git ignored so it's not committed to the repo
	*/
	//Config.AddConfigFile(".secrets.toml")

	Config.Init()
	return true
}`,
				file,
				file,
				prefix,
			),
		)

		configFile.Close()
		fmt.Printf(
			"\033[32m"+`• Done generating config !!!

`+"\033[36m$ tree\n.\n├──...\n├── config/"+`
│   └── config.go
├── %s.toml`+"\n...\033[0m"+`

`+"\033[33mUsage eg:"+`

// Example.go
import "<yourpackage>/config"

...
config.Init()
Config := config.Config

Config.Get(<toml key>)
...`+"\033[0m\n",
			file,
		)
	},
}

func init() {
	rootCmd.AddCommand(initCmd)
	initCmd.Flags().StringP("file", "f", "settings", "Filename (without extension) to use as config toml")
	initCmd.Flags().StringP("envprefix", "p", "ENV",
		`Prefix for environment variable overrides for Config. 
Eg: 
	`+"`ENV_TEST`"+` will be the name of the Env Var required to override `+"`TEST` in Config if the envprefix is set to 'ENV'",
	)
}
